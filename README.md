# Satisfactory-Shared

Service de partage de fichiers Satisfactory pour session multijoueurs serverless

## Description
Ce dépôt permet de partager et de versionner les fichier d'une instance Satisfactory entre différents joueurs.
Chaque joueur est invité à `pull` les fichiers pour lancer une instance et lacer un serveur en local. Ce seveur pourra être partagé pour que d'autre joueur puissent rejoindre l'hôte.

Ce dépôt met à dispositions des scripts pour

 * `pull` les derniers fichiers de jeu avant une session de jeu
 * `push` les derniers fichiers de jeu après une session de jeu

Ces scripts permettents notamment d'uniformiser le nom des commits.

## Utilisation

Étapes d'installation de Satisfactory-Shared

* Dans un terminal de type bash (git bash par exemple), lancez les commandes suivantes en remplissant les bloc prévus à cet effet.

```sh
    # Étape 1 - Récupération du dépôt

    ## Si vous avez la possibilité de clone en ssh
    git clone git@gitlab.com:ppuech/satisfactory-shared.git <VOTRE-EMPLACEMENT-CHEMIN-ABSOLU>

    ## Si non, vous pouvez clone en https
    git clone https://gitlab.com/ppuech/satisfactory-shared.git <VOTRE-EMPLACEMENT-CHEMIN-ABSOLU>

    ## Exemple
    git clone git@gitlab.com:ppuech/satisfactory-shared.git /c/projets/stasfactory-shared



    # Étape 2 - Créer votre branche personnelle
    ./satisfactory-shared.sh init

    ## Cette étape vous demandera de préciser
    ## * votre pseudo de jeu
    ## * L'emplacement de vos fichiers de jeu



    # Étape 3 - Préparation d'un lancement de jeu
    ./satisfactory-shared.sh pre-run



    # Étape 4 - Sauvegarde d'un session de jeu
    ./satisfactory-shared.sh pre-run

```

## Astuce

TODO - astuce icone de pull/push sur bureau
